﻿using System;

public class BodyOfMass
{
	private string name;
	private double radius, density;
	private Coordinate position, velocity;
	private double mass;
    private double mewStatic, mewKinetic;


	public BodyOfMass(string name, double radius, double density, Coordinate position, Coordinate velocity, Double mewStatic, Double mewKinetic) : base()
	{
		this.Name = name;
		this.Radius = radius;
		this.Density = density;
		this.Position = position;
		this.Velocity = velocity;
        this.mewStatic = mewStatic;
        this.mewKinetic = mewKinetic;
		this.mass = (4.0 / 3) * Math.PI * Math.Pow(this.radius, 3) * this.density;
	}


	public virtual Coordinate Velocity
	{
		get
		{
			return velocity;
		}
		set
		{
			this.velocity = value;
		}
	}




	public virtual double Mass
	{
		get
		{
			return mass;
		}
	}


	public virtual string Name
	{
		get
		{
			return name;
		}
		set
		{
			this.name = value;
		}
	}




	public virtual double Radius
	{
		get
		{
			return radius;
		}
		set
		{
			this.radius = value;
		}
	}




	public virtual double Density
	{
		get
		{
			return density;
		}
		set
		{
			this.density = value;
		}
	}




	public virtual Coordinate Position
	{
		get
		{
			return position;
		}
		set
		{
			this.position = value;
		}
	}

    public virtual double MewStatic
    {
        get
        {
            return mewStatic;
        }
        set
        {
            this.mewStatic = value;
        }
    }

    public virtual double MewKinetic
    {
        get
        {
            return mewKinetic;
        }
        set
        {
            this.mewKinetic = value;
        }
    }


    public override string ToString()
	{
		return "Projectile [name=" + name + ", radius=" + radius + ", density=" + density + ", position=" + position + ", velocity=" + velocity + "]";
	}




}
