﻿using System;

public class Coordinate
{
	private double x, y, z;

	public Coordinate(double x, double y, double z) : base()
	{
		this.X = x;
		this.Y = y;
		this.Z = z;
	}

	public virtual double X
	{
		get
		{
			return x;
		}
		set
		{
			this.x = value;
		}
	}


	public virtual double Y
	{
		get
		{
			return y;
		}
		set
		{
			this.y = value;
		}
	}


	public virtual double Z
	{
		get
		{
			return z;
		}
		set
		{
			this.z = value;
		}
	}

    

	public override string ToString()
	{
		return "\t" + x + "\t" + y + "\t" + z + "\t";
	}

	public virtual double DotProduct
	{
		get
		{
			return Math.Pow((Math.Pow(this.X, 2) + Math.Pow(this.Y, 2) + Math.Pow(this.Z, 2)), 0.5);
		}
	}

	public virtual double getDotProduct(Coordinate o)
	{
		return Math.Pow((this.X * o.X) + (this.Y * o.Y) + (this.Z * o.X), 0.5);
	}
}
