﻿public class Fluid
{
	private double c1, c2;

	public Fluid(double c1, double c2) : base()
	{
		this.c1 = c1;
		this.c2 = c2;
	}

	public virtual double C1
	{
		get
		{
			return c1;
		}
		set
		{
			this.c1 = value;
		}
	}


	public virtual double C2
	{
		get
		{
			return c2;
		}
		set
		{
			this.c2 = value;
		}
	}



	public override string ToString()
	{
		return "Fluid [c1=" + c1 + ", c2=" + c2 + "]";
	}

}
