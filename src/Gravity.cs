﻿using System;

public class Gravity
{
//JAVA TO C# CONVERTER NOTE: Fields cannot have the same name as methods:
	internal double G_Renamed = 0.0000000000667408;

	private BodyOfMass obj1, obj2;
	private double force, mass1, mass2, radius;

	public Gravity(BodyOfMass obj1, BodyOfMass obj2) : base()
	{

		this.Obj1 = obj1;
		this.Obj2 = obj2;
		this.mass1 = obj1.Mass;
		this.mass2 = obj2.Mass;
		this.radius = Math.Pow((Math.Pow(obj1.Position.X - obj2.Position.X, 2) + Math.Pow(obj1.Position.Y - obj2.Position.Y, 2) + Math.Pow(obj1.Position.Z - obj2.Position.Z, 2)), 0.5);
		this.force = (((this.mass1) / (Math.Pow(this.radius, 2))) * G_Renamed);
	}

	public virtual BodyOfMass Obj1
	{
		get
		{
			return obj1;
		}
		set
		{
			this.obj1 = value;
		}
	}


	public virtual BodyOfMass Obj2
	{
		get
		{
			return obj2;
		}
		set
		{
			this.obj2 = value;
		}
	}


	public virtual double G
	{
		get
		{
			return G_Renamed;
		}
	}

	public virtual double Force
	{
		get
		{
			return force;
		}
	}

	public virtual double Mass1
	{
		get
		{
			return mass1;
		}
	}

	public virtual double Mass2
	{
		get
		{
			return mass2;
		}
	}

	public virtual double Radius
	{
		get
		{
			return radius;
		}
	}

	public override string ToString()
	{
		return "Gravity [G=" + G_Renamed + ", obj1=" + obj1 + ", obj2=" + obj2 + ", force=" + force + ", mass1=" + mass1 + ", mass2=" + mass2 + ", radius=" + radius + "]";
	}
    
}
