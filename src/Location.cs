﻿public class Location
{
	private Coordinate position, velocity;
	private double time;
	public Location(Coordinate position, Coordinate velocity, double time) : base()
	{
		this.Position = position;
		this.Velocity = velocity;
		this.Time = time;
	}
	public virtual Coordinate Position
	{
		get
		{
			return position;
		}
		set
		{
			this.position = value;
		}
	}
	public virtual Coordinate Velocity
	{
		get
		{
			return velocity;
		}
		set
		{
			this.velocity = value;
		}
	}
	public virtual double Time
	{
		get
		{
			return time;
		}
		set
		{
			this.time = value;
		}
	}
	
	public override string ToString()
	{
		return "Time=" + time + "\nPosition=" + position + "\nVelocity=" + velocity;
	}


}
