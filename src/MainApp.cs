﻿using System;

public class MainApp
{
    public static void Main(string[] args)
    {
        MainApp theApp = new MainApp();
        theApp.start();
    }

    public virtual void start()
    {
        demoPhysicsEngine();
    }

    private void demoPhysicsEngine()
    {
        double initialTime = 5;
        double duration = 1.0 / 60;
        int accuracy = 1;
        Coordinate position = new Coordinate(4, -3, 8);
        Coordinate velocity = new Coordinate(-5, 10, 4);

        Coordinate current = new Coordinate(6, 5, 0);
        Coordinate rotation = new Coordinate((double)5 / 3, -((double)7 / 3), 4);

        string pName = "default-earth";
        double pRadius = 6371000;
        double pDensity = 5515.3;

        double fDensity = 120;
        double fCoefficient = 0.1;

        double bDensity = 7800;


        double mewStatic = 0.2;
        double mewKinetic = 1.3;

        Coordinate PointA = new Coordinate(2, 8, 2);
        Coordinate PointB = new Coordinate(8, 6, 3);
        Coordinate PointC = new Coordinate(3, 4, 9);
        Plane plane = new Plane(PointA, PointB, PointC);
        Console.WriteLine("Enter Planet Name (defaults include: 'earth','mars','moon','pluto') For custom planet enter anything:  ");
        pName = Console.ReadLine();

        if (pName == "earth")
        {
            pRadius = 6371000;
            pDensity = 5515.3;
            Console.WriteLine("\tDefault Earth Values");
            Console.WriteLine("Enter Planet Radius: " + pRadius);
            Console.WriteLine("Enter Planet Density: " + pDensity);
        } else if (pName == "mars")
        {
            pRadius = 3390000;
            pDensity = 3930;
            Console.WriteLine("\tDefault Mars Values");
            Console.WriteLine("Enter Planet Radius: " + pRadius);
            Console.WriteLine("Enter Planet Density: " + pDensity);
        }
        else if (pName == "moon")
        {
            pRadius = 1737000;
            pDensity = 3300;
            Console.WriteLine("\tDefault Moon Values");
            Console.WriteLine("Enter Planet Radius: " + pRadius);
            Console.WriteLine("Enter Planet Density: " + pDensity);
        }
        else if (pName == "pluto")
        {
            pRadius = 1186000;
            pDensity = 1880;
            Console.WriteLine("\tDefault Pluto Values");
            Console.WriteLine("Enter Planet Radius: " + pRadius);
            Console.WriteLine("Enter Planet Density: " + pDensity);
        }
        else
        { 
            Console.WriteLine("Enter Planet Radius: ");
            pRadius = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter Planet Density: ");
            pDensity = Convert.ToDouble(Console.ReadLine());
        }
        BodyOfMass planet = new BodyOfMass(pName, pRadius, pDensity, new Coordinate(0, 0, -pRadius), new Coordinate(0, 0, 0));

        Console.WriteLine("Enter Fluid Type (defaults include 'water','air','maplesyrup','oil') For custom type enter anything: ");
        string fName = Console.ReadLine();

        if (fName == "water")
        {
            Console.WriteLine("\tDefault Water Values");
            fDensity = 1000;
            Console.WriteLine("Enter Fluid Density: " + fDensity);
            Console.WriteLine("Enter Fluid Coefficient: ");
            fCoefficient = Convert.ToDouble(Console.ReadLine());
        }
        else if (fName == "air")
        {
            Console.WriteLine("\tDefault Air (at 20C and 1atm) Values");
            fDensity = 1.204;
            Console.WriteLine("Enter Fluid Density: " + fDensity);
            Console.WriteLine("Enter Fluid Coefficient: ");
            fCoefficient = Convert.ToDouble(Console.ReadLine());
        }
        else if (fName == "maplesyrup")
        {
            Console.WriteLine("\tDefault Mayple Syrup Values");
            fDensity = 1362;
            Console.WriteLine("Enter Fluid Density: " + fDensity);
            Console.WriteLine("Enter Fluid Coefficient: ");
            fCoefficient = Convert.ToDouble(Console.ReadLine());
        }
        else if (fName == "oil")
        {
            Console.WriteLine("\tDefault Olive Oil Values");
            fDensity = 913;
            Console.WriteLine("Enter Fluid Density: "+fDensity);
            Console.WriteLine("Enter Fluid Coefficient: ");
            fCoefficient = Convert.ToDouble(Console.ReadLine());
        }
        else
        {
            Console.WriteLine("\tEnter Custom Values");
            Console.WriteLine("Enter Fluid Density: ");
            fDensity = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter Fluid Coefficient: ");
            fCoefficient = Convert.ToDouble(Console.ReadLine());
        }

        Console.WriteLine("Enter Fluid Current:\ni: ");
        double fCi = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("j: ");
        double fCj = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("k: ");
        double fCk = Convert.ToDouble(Console.ReadLine());

        current = new Coordinate(fCi, fCj, fCk);
        Fluid fluid = new Fluid(fDensity, fCoefficient);

        Console.WriteLine("Enter Ball Material (Defaults include 'steel','wood','ice'): For custom material enter anything: ");
        string bName = Console.ReadLine();

        if (bName == "steel")
        {
            Console.WriteLine("\tDefault Steel Values");
            bDensity = 7800;
            Console.WriteLine("Enter Ball Density: "+bDensity);
        }
        else if (bName == "wood")
        {
            Console.WriteLine("\tDefault Pine Wood Values");
            bDensity = 510;
            Console.WriteLine("Enter Ball Density: " + bDensity);
        }
        else if (bName == "ice")
        {
            Console.WriteLine("\tDefault Ice Values");
            bDensity = 850;
            Console.WriteLine("Enter Ball Density: " + bDensity);
        }
        else
        {
            Console.WriteLine("Enter Ball Density: ");
            bDensity = Convert.ToDouble(Console.ReadLine());
        }


        Console.WriteLine("Enter Ball Radius: ");
        double bRadius = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Enter Ball Position:\ni: ");
        double bPi = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("j: ");
        double bPj = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("k: ");
        double bPk = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Enter Ball Velocity:\ni: ");
        double bVi = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("j: ");
        double bVj = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("k: ");
        double bVk = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Enter Ball Rotation:\ni: ");
        double bRi = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("j: ");
        double bRj = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("k: ");
        double bRk = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Enter duration: ");
        duration = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Enter steps: ");
        accuracy = Convert.ToInt16(Console.ReadLine());
        
        position = new Coordinate(bPi,bPj,bPk);
        velocity = new Coordinate(bVi,bVj,bVk);
        rotation = new Coordinate(bVi, bVj, bVk);

        
        BodyOfMass ball = new BodyOfMass(bName, bRadius, bDensity, position, velocity, mewStatic, mewKinetic);
        Gravity g = new Gravity(planet, ball);


        Console.WriteLine("\tPlanet\nName:\t"+planet.Name+"\nMass:\t"+planet.Mass+"\n\tBall\nName:\t"+ball.Name + "\nMass:\t" + ball.Mass + "\nGravity:\t"+g.Force);
        


        translateProjectile(initialTime, duration, ball, g, fluid, current, rotation, accuracy, plane);

        Console.ReadLine();
    }

    public virtual void translateProjectile(double initialTime, double time, BodyOfMass obj, Gravity g, Fluid f, Coordinate current, Coordinate rotation, int accuracy, Plane plane)
    {
        Coordinate velocity = obj.Velocity, position = obj.Position, acc = acceleration(obj, rotation, f, current, g, plane);
        double t, h;
        t = initialTime;
        h = (time / accuracy);

        //double positionX, positionY, positionZ, velocityX, velocityY, velocityZ;



        Location location = new Location(position, velocity, t);
        Console.WriteLine(obj.Name + "\n" + location);

        //		Euler's Method
        /*		for(int i = 0; i<accuracy; i++)
        		{
        			
        			position = obj.Position;
        		    velocity = obj.Velocity;
        		
        			positionX=(position.X+velocity.X*h);
        			positionY=(position.Y+velocity.Y*h);
        			positionZ=(position.Z+velocity.Z*h);
        			velocityX=(velocity.X+acc.X*h);
        			velocityY=(velocity.Y+acc.Y*h);
        			velocityZ=(velocity.Z+acc.Z*h);
        			position =new Coordinate(positionX, positionY, positionZ);
        			velocity = new Coordinate(velocityX,velocityY,velocityZ);
        			obj.Position=(new Coordinate(positionX, positionY, positionZ));
        			obj.Velocity=(new Coordinate(velocityX, velocityY, velocityZ));
        			acc = acceleration(obj, rotation, f, current, g);
        			t+=h;
        			
        			Location currentLocation = new Location(position, velocity, t);
                    Console.WriteLine(obj.Name+"\n"+currentLocation);
        		}
*/

        double check = t + time;
        //		Runge Kutta
        while (check >= t)
        {
            t = t + h;
            if (obj.Position.Z > obj.Radius)
            {
                obj = rungeKutta(obj, rotation, f, current, g, h, plane);
            }
            Location currentLocation = new Location(obj.Position, obj.Velocity, (Math.Round(t * 10000000.0) / 10000000.0));
            Console.WriteLine(obj.Name + "\n" + currentLocation);
        }
        
    }

    public virtual Coordinate acceleration(BodyOfMass obj, Coordinate rotation, Fluid f, Coordinate current, Gravity gravity, Plane plane)
    {
        Coordinate d = new Coordinate(0, 0, 0);
        Coordinate m = new Coordinate(0, 0, 0);
        Coordinate forceF = new Coordinate(0, 0, 0);
        Coordinate g = new Coordinate(0, 0, -obj.Mass * 9.81); //default gravity value
        if (!checkIfOnPlane(obj, plane))
        {
            d = drag(obj, f, current);
            m = magnus(obj, rotation, f, current);
        }
        else
        {
            Coordinate gp = gravityParallel(g, plane);
            //Friction
            forceF = friction(g, gp, obj, plane);
            g = gp;
        }
        //System.out.println("Magnus: "+m);
        //Coordinate g = new Coordinate(0, 0, -obj.Mass * gravity.Force);

        Coordinate a = new Coordinate(g.X + d.X + m.X + forceF.X, g.Y + d.Y + m.Y + forceF.Y, g.Z + d.Z + m.Z + forceF.Z);

        double mass = 1 / obj.Mass;
        a.X = a.X * mass;
        a.Y = a.Y * mass;
        a.Z = a.Z * mass;
        return a;
    }

    public virtual Coordinate friction(Coordinate g, Coordinate gp, BodyOfMass obj, Plane plane)
    {
        Coordinate gNormal = gravityNormal(g, plane);
        Coordinate fNormal = new Coordinate(-1 * gNormal.X, -1 * gNormal.Y, -1 * gNormal.Z);
        Double forceNormal = Math.Pow(Math.Pow(fNormal.X,2)+Math.Pow(fNormal.Y,2)+Math.Pow(fNormal.Z,2), 0.5);
        Double frictionMax = obj.MewStatic*forceNormal;
        if (isStatic(frictionMax, obj, gp))
        {
            return new Coordinate(-1*gp.X, -1*gp.Y, -1*gp.Z);
        }
        else
        {
            Double friction = obj.MewKinetic * forceNormal;
            return new Coordinate(gp.X*friction, gp.Y*friction, gp.Z*friction);
        }
    }

    private bool isStatic(Double frictionMax, BodyOfMass obj, Coordinate gp)
    {
        Coordinate tForce = new Coordinate(obj.Velocity.X+gp.X, obj.Velocity.Y + gp.Y, obj.Velocity.Z + gp.Z);
        Double totalForce = Math.Pow(Math.Pow(tForce.X, 2) + Math.Pow(tForce.Y, 2) + Math.Pow(tForce.Z, 2), 0.5);
        if (frictionMax >= totalForce)
            return true;
        else
            return false;
    }

    public virtual Coordinate gravityParallel(Coordinate g, Plane plane)
    {
        //gn = [g.N]*N
        //gp = g-gn
        //[g.N] => gN = g.X*plane.normal.X+g.Y*plane.normal.Y+g.Z*plane.normal.Z
        //[g.N]*N => gn = Coordinate(gN*plane.normal.X,gN*plane.normal.Y,gN*plane.normal.Z);
        //g-gn => gp = Coordinate(g.X-gn.X,g.Y-gn.Y,g.Z-gn.Z);
        Coordinate gn = gravityNormal(g, plane);
        return new Coordinate(g.X - gn.X, g.Y - gn.Y, g.Z - gn.Z);
    }
    public virtual Coordinate gravityNormal(Coordinate g, Plane plane)
    {
        Double gN = g.X * plane.Normal.X + g.Y * plane.Normal.Y + g.Z * plane.Normal.Z;
        return new Coordinate(gN * plane.Normal.X, gN * plane.Normal.Y, gN * plane.Normal.Z);
    }
    public virtual Boolean checkIfOnPlane(BodyOfMass obj, Plane plane)
    {
        double planeZCoordinate = 
            plane.C1.Z + 
            ((((plane.C2.X-plane.C1.X)*(plane.C3.Z-plane.C1.Z))
            -((plane.C3.X-plane.C1.X)*(plane.C2.Z-plane.C1.Z))
            /((plane.C2.X-plane.C1.X)*(plane.C3.Y-plane.C1.Y))
            -((plane.C3.X-plane.C1.X)*(plane.C2.Y-plane.C1.Y)))
            *(obj.Position.Y-plane.C1.Y))
            -((((plane.C2.Y-plane.C1.Y)*(plane.C3.Z-plane.C1.Z))
            -((plane.C3.Y-plane.C1.Y)*(plane.C2.Z-plane.C1.Z))
            /((plane.C2.X-plane.C1.X)*(plane.C3.Y-plane.C1.Y))
            -((plane.C3.X-plane.C1.X)*(plane.C2.Y-plane.C1.Y)))
            *(obj.Position.X-plane.C1.X));

        if (obj.Position.Z > planeZCoordinate)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public virtual Coordinate drag(BodyOfMass obj, Fluid f, Coordinate current)
    {
        Coordinate velocity = obj.Velocity;
        Coordinate result = new Coordinate(0, 0, 0), v = new Coordinate(velocity.X - current.X, velocity.Y - current.Y, velocity.Z - current.Z);

        double d = 0.5 * f.C1 * Math.PI * (Math.Pow(obj.Radius, 2));
        double drag = -d * f.C2 * v.DotProduct;

        result.X = v.X * drag;
        result.Y = v.Y * drag;
        result.Z = v.Z * drag;
        return result;
    }

    public virtual Coordinate magnus(BodyOfMass obj, Coordinate rotation, Fluid f, Coordinate current)
    {
        Coordinate velocity = obj.Velocity;
        Coordinate v = new Coordinate(velocity.X - current.X, velocity.Y - current.Y, velocity.Z - current.Z);
        Coordinate magnus = new Coordinate(0, 0, 0);
        Coordinate cross = matrixCrossProduct(rotation, v);
        double r = obj.Radius;
        double d = 0.5 * f.C1 * Math.PI * (Math.Pow(r, 2));
        double w = rotation.DotProduct;

        magnus.X = d * r * w * (cross.X / cross.DotProduct);
        magnus.Y = d * r * w * (cross.Y / cross.DotProduct);
        magnus.Z = d * r * w * (cross.Z / cross.DotProduct);

        return magnus;
    }

    public virtual Coordinate matrixCrossProduct(Coordinate c1, Coordinate c2)
    {
        return new Coordinate((c1.Y * c2.Z) - (c1.Z * c2.Y), -((c1.X * c2.Z) - (c1.Z * c2.X)), (c1.X * c2.Y) - (c1.Y * c2.X));
    }

    public virtual BodyOfMass rungeKutta(BodyOfMass obj, Coordinate rotation, Fluid f, Coordinate current, Gravity g, double h, Plane plane)
    {
        BodyOfMass temp = obj;
        Coordinate position = obj.Position;

        Coordinate velocity = obj.Velocity;
        Coordinate v1 = obj.Velocity;

        Coordinate a1 = acceleration(obj, rotation, f, current, g, plane);

        double p2x = position.X + (0.5 * v1.X * h);
        double p2y = position.Y + (0.5 * v1.Y * h);
        double p2z = position.Z + (0.5 * v1.Z * h);
        Coordinate p2 = new Coordinate(p2x, p2y, p2z);

        double v2x = velocity.X + (0.5 * a1.X * h);
        double v2y = velocity.Y + (0.5 * a1.Y * h);
        double v2z = velocity.Z + (0.5 * a1.Z * h);
        Coordinate v2 = new Coordinate(v2x, v2y, v2z);

        obj.Position = p2;
        obj.Velocity = v2;
        Coordinate a2 = acceleration(obj, rotation, f, current, g, plane);

        double p3x = position.X + (0.5 * v2.X * h);
        double p3y = position.Y + (0.5 * v2.Y * h);
        double p3z = position.Z + (0.5 * v2.Z * h);
        Coordinate p3 = new Coordinate(p3x, p3y, p3z);

        double v3x = velocity.X + (0.5 * a2.X * h);
        double v3y = velocity.Y + (0.5 * a2.Y * h);
        double v3z = velocity.Z + (0.5 * a2.Z * h);
        Coordinate v3 = new Coordinate(v3x, v3y, v3z);

        obj.Position = p3;
        obj.Velocity = v3;
        Coordinate a3 = acceleration(obj, rotation, f, current, g, plane);

        double p4x = position.X + (v3.X * h);
        double p4y = position.Y + (v3.Y * h);
        double p4z = position.Z + (v3.Z * h);
        Coordinate p4 = new Coordinate(p4x, p4y, p4z);

        double v4x = velocity.X + (a3.X * h);
        double v4y = velocity.Y + (a3.Y * h);
        double v4z = velocity.Z + (a3.Z * h);
        Coordinate v4 = new Coordinate(v4x, v4y, v4z);

        obj.Position = p4;
        obj.Velocity = v4;
        Coordinate a4 = acceleration(obj, rotation, f, current, g, plane);

        double pFx = position.X + ((h / 6) * (v1.X + (2 * v2x) + (2 * v3x) + v4x));
        double pFy = position.Y + ((h / 6) * (v1.Y + (2 * v2y) + (2 * v3y) + v4y));
        double pFz = position.Z + ((h / 6) * (v1.Z + (2 * v2z) + (2 * v3z) + v4z));
        Coordinate pF = new Coordinate(pFx, pFy, pFz);

        double vFx = velocity.X + ((h / 6) * (a1.X + (2 * a2.X) + (2 * a3.X) + a4.X));
        double vFy = velocity.Y + ((h / 6) * (a1.Y + (2 * a2.Y) + (2 * a3.Y) + a4.Y));
        double vFz = velocity.Z + ((h / 6) * (a1.Z + (2 * a2.Z) + (2 * a3.Z) + a4.Z));
        Coordinate vF = new Coordinate(vFx, vFy, vFz);

        obj.Position = pF;
        obj.Velocity = vF;

        if (obj.Position.Z < 0)
        {
            obj = temp;
            obj.Velocity = new Coordinate(0, 0, 0);
            double x = pF.X;
            double y = pF.Y;
            obj.Position = new Coordinate(x, y, obj.Radius);
        }
        return obj;
    }
}

