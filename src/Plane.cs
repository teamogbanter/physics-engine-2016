﻿using System;

public class Plane
{
    private Coordinate c1, c2, c3, normal, a, b;

    public Plane(Coordinate c1, Coordinate c2, Coordinate c3) : base()
	{
        this.C1 = c1;
        this.C2 = c2;
        this.C3 = c3;
        this.a = new Coordinate(c2.X - c1.X, c2.Y - c2.Y, c3.Z - c3.Z);
        this.b = new Coordinate(c3.X - c1.X, c3.Y - c1.Y, c3.Z - c1.Z); 
        this.normal = new Coordinate((a.Y * b.Z) - (a.Z * b.Y), -((a.X * b.Z) - (a.Z * b.X)), (a.X * b.Y) - (a.Y * b.X));
    }

    public virtual Coordinate C1
    {
        get
        {
            return c1;
        }
        set
        {
            this.c1 = value;
        }
    }

    public virtual Coordinate C2
    {
        get
        {
            return c2;
        }
        set
        {
            this.c2 = value;
        }
    }

    public virtual Coordinate C3
    {
        get
        {
            return c3;
        }
        set
        {
            this.c3 = value;
        }
    }

    public virtual Coordinate Normal
    {
        get
        {
            return normal;
        }
        set
        {
            this.normal = value;
        }
    }
}
